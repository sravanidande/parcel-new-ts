import React from 'react'
import { useImmer } from 'use-immer'

function getRandomIntInclusive(min: number, max: number) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1) + min) //The maximum is inclusive and the minimum is inclusive
}
type Player = 0 | 1

interface PlayerTurnProps {
  readonly player: Player
}

export const PlayerTurn: React.FC<PlayerTurnProps> = ({ player }) => {
  return <h3>{player === 0 ? 'player1 turn' : 'player2 turn '} </h3>
}

interface PlayerDiceProps {
  readonly dice: number
  readonly score: number
}
export const PlayerDice: React.FC<PlayerDiceProps> = ({ dice, score }) => {
  return (
    <>
      <h3>score:{score}</h3>
      <button>{dice}</button>
    </>
  )
}

interface GameButtonProps {
  readonly dice: number
  onDiceRoll(dice: number): void
  readonly winner?: Player
}

export const GameButton: React.FC<GameButtonProps> = ({
  onDiceRoll,
  dice,
  winner,
}) => {
  return (
    <button onClick={() => onDiceRoll(dice)}>
      {winner !== undefined ? 'Restart game' : 'Roll Dice'}
    </button>
  )
}

interface GameWinnerProps {
  readonly winner: Player
}

export const GameWinner: React.FC<GameWinnerProps> = ({ winner }) => {
  return <div>{winner} won</div>
}

function otherPlayer(player: Player): Player {
  return player === 0 ? 1 : 0
}
interface DiceState {
  readonly player: Player
  readonly winner?: Player
  readonly dice: [number, number]
  readonly score: [number, number]
}

const initialState: DiceState = {
  player: 0,
  dice: [0, 0],
  score: [0, 0],
}

export const DiceGame: React.FC = () => {
  const [{ player, score, dice, winner }, set] = useImmer(initialState)

  const handleRollDice = () => {
    const r = getRandomIntInclusive(1, 6)

    set(state => {
      if (state.winner !== undefined) {
        return initialState
      }
      if (state.score[state.player] + r >= 20) {
        state.winner = state.player
      }
      state.dice[state.player] = r
      state.score[state.player] = state.score[state.player] + r

      state.player = otherPlayer(state.player)
      return undefined
    })
  }

  return (
    <div>
      <PlayerTurn player={player} />
      <div style={{ margin: '40px' }}>
        {[0, 1].map(i => (
          <PlayerDice key={i} score={score[i]} dice={dice[i]} />
        ))}
      </div>
      <div style={{ margin: '20px' }}>
        <GameButton dice={6} onDiceRoll={handleRollDice} winner={winner} />
      </div>
      {winner !== undefined && <GameWinner winner={winner} />}
    </div>
  )
}
