export const bands: string[] = [
  'The Plot in You',
  'The Devil Wears Prada',
  'Pierce the Veil',
  'Norma Jean',
  'The Bled',
  'Say Anything',
  'The Midway State',
  'We Came as Romans',
  'Counterparts',
  'Oh, Sleeper',
  'A Skylit Drive',
  'Anywhere But Here',
  'An Old Dog',
]

export const sortWithoutArticles = (arr: string[]): string[] => {
  let result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (
      arr[i].includes('A') ||
      arr[i].includes('An') ||
      arr[i].includes('The')
    ) {
      result.push(arr[i].split(' ').slice(1).join(' '))
    } else {
      result.push(arr[i])
    }
  }
  return result
}

// const exampleTuple: [string, number] = ['hello', 1]
