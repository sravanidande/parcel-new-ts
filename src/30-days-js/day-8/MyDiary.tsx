import React from 'react'
import './App.css'

interface DiaryEntryProps {
  onAddEntry(entry: string): void
}

export const DiaryEntry: React.FC<DiaryEntryProps> = ({ onAddEntry }) => {
  const [text, setText] = React.useState('')
  return (
    <>
      <h1 className="header">My Personal Diary</h1>
      <form className="form">
        <textarea
          className="textarea"
          placeholder="Type here.."
          value={text}
          onChange={evt => setText(evt.target.value)}
        />
        <br />
        <button
          className="button"
          type="submit"
          onClick={evt => {
            evt.preventDefault()
            onAddEntry(text)
            setText('')
          }}
        >
          submit
        </button>
      </form>
    </>
  )
}

interface DisplayLogProps {
  onLogEntry(index: number): void
  readonly count: number
  readonly log: string
  readonly selected: boolean
}

export const DisplayLog: React.FC<DisplayLogProps> = ({
  count,
  onLogEntry,
  log,
  selected,
}) => {
  return (
    <>
      <button
        className="log-button"
        onClick={() => {
          onLogEntry(count)
        }}
      >
        {count + 1}
      </button>
      {selected ? <div>{log}</div> : null}
    </>
  )
}

interface DiaryLogProps {
  readonly logs: readonly string[]
  onEntryLog(index: number): void
  readonly selectedIndex: number
}

export const DiaryLog: React.FC<DiaryLogProps> = ({
  logs,
  onEntryLog,
  selectedIndex,
}) => {
  return (
    <section>
      <div className="entry">
        <div>
          {logs.map((log, index) => (
            <div key={log}>
              <DisplayLog
                log={log}
                onLogEntry={onEntryLog}
                count={index}
                selected={selectedIndex === index}
              />
            </div>
          ))}
        </div>
      </div>
    </section>
  )
}

export const MyDiary = () => {
  const [logs, setLogs] = React.useState<string[]>([])
  const [selectedIndex, setSelectedIndex] = React.useState(-1)

  const handleLogs = (entry: string) => {
    setLogs([...logs, entry])
  }

  const handleLogEntry = (index: number) => {
    setSelectedIndex(index)
  }

  return (
    <>
      <DiaryEntry onAddEntry={handleLogs} />
      <DiaryLog
        logs={logs}
        onEntryLog={handleLogEntry}
        selectedIndex={selectedIndex}
      />
    </>
  )
}
