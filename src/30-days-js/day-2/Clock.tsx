import React from 'react'
import './App.css'

// eslint-disable-next-line arrow-body-style
export const Clock: React.FC = () => {
  const [date, setDate] = React.useState(new Date())
  const [pause, setPause] = React.useState(false)

  console.log('rendered')

  const handlePause = () => {
    setPause(!pause)
  }

  const handleSeconds = () => {
    const now = new Date()
    setDate(now)
  }

  React.useEffect(() => {
    if (!pause) {
      const handle = setInterval(handleSeconds, 1000)
      return () => clearInterval(handle)
    }
    return undefined
  }, [date, pause])

  return (
    <>
      <div className="clock">
        <h1>{date.getHours()}:</h1>
        <h1>{date.getMinutes()}:</h1>
        <h1>{date.getSeconds()}</h1>
      </div>
      <button onClick={handlePause}>{!pause ? 'pause' : 'start'}</button>
    </>
  )
}
