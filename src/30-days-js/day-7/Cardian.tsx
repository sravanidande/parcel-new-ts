import React from 'react'

interface CardianProps {
  onDisable(index: number): void
}

export const Cardian: React.FC<CardianProps> = () => {
  const [text, setText] = React.useState('')
  return (
    <>
      <button
        style={{ backgroundColor: 'skyblue' }}
        onClick={() => setText('React')}
      >
        React
      </button>
      <button
        style={{ backgroundColor: 'lightgreen' }}
        onClick={() => setText('Javascript')}
      >
        JS
      </button>
      <button
        style={{ backgroundColor: 'lightpink' }}
        onClick={() => setText('Typescript')}
      >
        TS
      </button>
      <div>
        <p>choose one:{text}</p>
      </div>
    </>
  )
}
