import { addMonths, getDaysInMonth, startOfMonth, subMonths } from 'date-fns'
import { range } from 'lodash'
import React from 'react'

export const empties = (n: number): string[] => {
  return range(n).map(_ => '')
}

export function divideRowsByNumber<T>(arr: T[], value: number): T[][] {
  let result = []
  for (let i = 0; i < arr.length; i += value) {
    result.push(arr.slice(i, i + value))
  }
  return result
}

const years = range(1980, 2030 + 1)

interface YearPickerProps {
  onYearSelect(year: number): void
}

export const YearPicker: React.FC<YearPickerProps> = ({ onYearSelect }) => {
  return (
    <select onChange={evt => onYearSelect(+evt.target.value)}>
      {years.map((year, index) => (
        <option key={index} value={year}>
          {year}
        </option>
      ))}
    </select>
  )
}

const dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

const monthNames = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
]

interface DatePickerViewProps {
  readonly date: DatePickerValue
  onDateChange(date: DatePickerValue): void
}

const toDatePickerValue = (date: Date) => ({
  day: date.getDate(),
  month: date.getMonth(),
  year: date.getFullYear(),
})

export const DatePickerView: React.FC<DatePickerViewProps> = ({
  date: { day, month, year },
  onDateChange,
}) => {
  const date = new Date(year, month, day)

  const firstDayOfMonth = startOfMonth(date).getDay()

  const all = empties(firstDayOfMonth).concat(
    range(1, getDaysInMonth(date) + 1).map(v => v.toString()),
  )
  const rows = divideRowsByNumber(all, dayNames.length)

  return (
    <>
      <div style={{ display: 'flex' }}>
        <button
          onClick={() => onDateChange(toDatePickerValue(subMonths(date, 1)))}
        >
          ⏪
        </button>
        <div>
          <h2>{`${monthNames[month]},${year}`}</h2>
        </div>
        <button
          onClick={() => onDateChange(toDatePickerValue(addMonths(date, 1)))}
        >
          ⏩
        </button>
      </div>

      <table style={{ display: 'unset' }}>
        <thead>
          <tr>
            {dayNames.map((d, index) => (
              <th key={index}>{d}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((dates, i) => (
            <tr key={i}>
              {dates.map((date, i) => (
                <td key={i}>{date}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export interface DatePickerValue {
  readonly day: number
  readonly month: number
  readonly year: number
}
interface DatePickerProps {
  readonly date: DatePickerValue
  onDateChange(date: DatePickerValue): void
}

export const DatePicker: React.FC<DatePickerProps> = ({
  date,
  onDateChange,
}) => {
  const [popup, setPopup] = React.useState(false)

  return (
    <>
      <div>
        <YearPicker onYearSelect={year => onDateChange({ ...date, year })} />
        <button onClick={() => setPopup(!popup)}>📅</button>
      </div>
      {popup ? (
        <DatePickerView date={date} onDateChange={onDateChange} />
      ) : null}
    </>
  )
}
