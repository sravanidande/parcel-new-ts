export interface Inventor {
  readonly first: string
  readonly last: string
  readonly year: number
  readonly passed: number
}

export const inventors: Inventor[] = [
  { first: 'Albert', last: 'Einstein', year: 1879, passed: 1955 },
  { first: 'Isaac', last: 'Newton', year: 1643, passed: 1727 },
  { first: 'Galileo', last: 'Galilei', year: 1564, passed: 1642 },
  { first: 'Marie', last: 'Curie', year: 1867, passed: 1934 },
  { first: 'Johannes', last: 'Kepler', year: 1571, passed: 1630 },
  { first: 'Nicolaus', last: 'Copernicus', year: 1473, passed: 1543 },
  { first: 'Max', last: 'Planck', year: 1858, passed: 1947 },
  { first: 'Katherine', last: 'Blodgett', year: 1898, passed: 1979 },
  { first: 'Ada', last: 'Lovelace', year: 1815, passed: 1852 },
  { first: 'Sarah E.', last: 'Goode', year: 1855, passed: 1905 },
  { first: 'Lise', last: 'Meitner', year: 1878, passed: 1968 },
  { first: 'Hanna', last: 'Hammarström', year: 1829, passed: 1909 },
]

export const people: string[] = [
  'Bernhard, Sandra',
  'Bethea, Erin',
  'Becker, Carl',
  'Bentsen, Lloyd',
  'Beckett, Samuel',
  'Blake, William',
  'Berger, Ric',
  'Beddoes, Mick',
  'Beethoven, Ludwig',
  'Belloc, Hilaire',
  'Begin, Menachem',
  'Bellow, Saul',
  'Benchley, Robert',
  'Blair, Robert',
  'Benenson, Peter',
  'Benjamin, Walter',
  'Berlin, Irving',
  'Benn, Tony',
  'Benson, Leana',
  'Bent, Silas',
  'Berle, Milton',
  'Berry, Halle',
  'Biko, Steve',
  'Beck, Glenn',
  'Bergman, Ingmar',
  'Black, Elk',
  'Berio, Luciano',
  'Berne, Eric',
  'Berra, Yogi',
  'Berry, Wendell',
  'Bevan, Aneurin',
  'Ben-Gurion, David',
  'Bevel, Ken',
  'Biden, Joseph',
  'Bennington, Chester',
  'Bierce, Ambrose',
  'Billings, Josh',
  'Birrell, Augustine',
  'Blair, Tony',
  'Beecher, Henry',
  'Biondo, Frank',
]

export const filterInventors = (arr: Inventor[]): Inventor[] =>
  arr.filter(val => val.year >= 1500 && val.year <= 1599)

export const returnNames = (arr: Inventor[]): string[] =>
  arr.map(val => `${val.first} ${val.last}`)

export const sortBirthDays = (arr: Inventor[]): Inventor[] =>
  arr.sort((val1, val2) => val1.year - val2.year)

export const sumUpInstances = (arr: string[]) => {
  const result = new Map<string, number>()
  for (let i = 0; i < arr.length; i += 1) {
    const value = result.get(arr[i])
    result.set(arr[i], value ? value + 1 : 1)
  }
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return result
}

export const sortByLastName = (arr: string[]): string[] =>
  arr
    .map(v => v.split(','))
    .map(v => v[1])
    .sort()

export const containsName = (arr: string[]): string[] =>
  arr.filter(v => v.includes('le'))

export const sortByYearsLived = (arr: Inventor[]): number[] =>
  arr.map(v => v.year).sort()

export const liveAllTogether = (arr: Inventor[]): number =>
  arr.map((_, index) => index + 1).reduce((acc, v) => acc + v, 0)
