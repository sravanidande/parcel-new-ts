import React from 'react'
import { DrumKey } from './data'
import './App.css'

interface DrumKeyViewProps {
  readonly drumKey: DrumKey
  readonly playing: boolean
  onTransitionEnd(evt: React.TransitionEvent<HTMLDivElement>): void
}

export const DrumKeyView: React.FC<DrumKeyViewProps> = ({
  drumKey,
  playing,
  onTransitionEnd,
}) => (
  <div
    className={playing ? 'key playing' : 'key'}
    onTransitionEnd={onTransitionEnd}
  >
    <kbd>{drumKey.name}</kbd>
    <span className="sound">{drumKey.sound}</span>
  </div>
)

const audioUrls = {
  '65': 'sounds/clap.wav',
  '83': 'sounds/hihat.wav',
  '68': 'sounds/kick.wav',
  '70': 'sounds/openhat.wav',
  '71': 'sounds/boom.wav',
  '72': 'sounds/ride.wav',
  '74': 'sounds/snare.wav',
  '75': 'sounds/tom.wav',
  '76': 'sounds/tink.wav',
}

const keys = Object.keys(audioUrls)

export const DrumkitView: React.FC<{
  readonly drumKeys: readonly DrumKey[]
}> = ({ drumKeys }) => {
  const [key, set] = React.useState<string | undefined>(undefined)
  const removeTransition = (e: React.TransitionEvent<HTMLDivElement>) => {
    if (e.propertyName !== 'transform') {
      return
    }
    set(undefined)
  }

  return (
    <div
      className="keys"
      tabIndex={0}
      onKeyDown={async e => {
        const key = e.keyCode.toString()
        if (keys.includes(key)) {
          set(key)
          await new Audio(audioUrls[key]).play()
        }
      }}
    >
      {drumKeys.map((d: DrumKey) => (
        <DrumKeyView
          key={d.key}
          drumKey={d}
          playing={d.key.toString() === key}
          onTransitionEnd={removeTransition}
        />
      ))}
    </div>
  )
}
