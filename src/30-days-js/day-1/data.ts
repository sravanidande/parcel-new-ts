
export interface DrumKey {
  readonly key: number
  readonly name: string
  readonly sound: string
}


export const drumKeys:ReadonlyArray<DrumKey> = [{ key: 65, name: "A", sound: "clap" },
  { key: 83, name: "S", sound: "hihat" },
  { key: 68, name: "D", sound: "kick" },
  { key: 70, name: "F", sound: "openhat" },
  { key: 71, name: "G", sound: "boom" },
  { key: 72, name: "H", sound: "ride" },
  { key: 74, name: "J", sound: "snare" },
  { key: 75, name: "K", sound: "tom" },
  { key: 76, name: "L", sound: "tink" }
  ]
