import React from 'react'
import './App.css'

interface TimerButtonsProps {
  onTimerChange(sec: number): void
}

export const TimerButtons: React.FC<TimerButtonsProps> = ({
  onTimerChange,
}) => {
  return (
    <div className="buttons">
      <button
        className="button"
        onClick={() => {
          onTimerChange(20)
        }}
      >
        20 SECS
      </button>
      <button className="button" onClick={() => onTimerChange(300)}>
        WORK 5
      </button>
      <button className="button" onClick={() => onTimerChange(900)}>
        QUICK 15
      </button>
      <button className="button" onClick={() => onTimerChange(1200)}>
        SNACK 20
      </button>
      <button className="button" onClick={() => onTimerChange(3600)}>
        LUNCH BREAK
      </button>
      <input
        className="input"
        type="number"
        onChange={evt => {
          if (evt.target.value.trim().length === 0) return
          onTimerChange(+evt.target.value * 60)
        }}
        placeholder="enter minutes..."
      />
    </div>
  )
}

interface TimerDisplayProps {
  readonly initialSeconds: number
}

export const useMount = () => {
  const [mounted, set] = React.useState(true)
  React.useEffect(() => {
    return () => {
      set(false)
    }
  }, [])

  return mounted
}

export const TimerDisplay: React.FC<TimerDisplayProps> = ({
  initialSeconds,
}) => {
  const [seconds, setSeconds] = React.useState(initialSeconds)
  const then = React.useMemo(() => Date.now() + initialSeconds * 1000, [])
  const mounted = useMount()

  React.useEffect(() => {
    setTimeout(() => {
      const secondsLeft = Math.round((then - Date.now()) / 1000)
      if (secondsLeft >= 0) {
        if (mounted) {
          setSeconds(secondsLeft)
        }
      }
    }, 1000)
  }, [seconds])

  const minutes = Math.floor(seconds / 60)
  const remainingSeconds = seconds - minutes * 60
  return (
    <h1 className="title">{`${minutes}:${
      remainingSeconds < 10 ? '0' : ''
    }${remainingSeconds}`}</h1>
  )
}

export const CountDownTimer = () => {
  const [timer, setTimer] = React.useState(0)
  console.log(timer)
  return (
    <div className="display">
      <TimerButtons onTimerChange={setTimer} />
      <TimerDisplay key={timer} initialSeconds={timer} />
    </div>
  )
}
