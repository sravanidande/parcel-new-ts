import React from 'react'

export const TweetNote: React.FC = () => {
  const [text, setText] = React.useState('')
  const total = 10
  return (
    <>
      <textarea
        rows={5}
        value={text}
        onChange={evt => {
          if (text.length > total) {
            alert('make sure text is less 10 characters')
          } else {
            setText(evt.target.value)
          }
        }}
      />
      <p>character count:{text.length}</p>
      <div>
        <button disabled={text.length > total}>tweet</button>
      </div>
    </>
  )
}
