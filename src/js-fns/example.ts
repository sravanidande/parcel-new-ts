// import { Inventor } from '../App'

// import { getDaysInMonth, startOfMonth } from 'date-fns'

// export const removeDuplicates = (arr: number[]): number[] => {
//   const result: number[] = []
//   arr.forEach(n => {
//     if (!result.includes(n)) {
//       result.push(n)
//     }
//   })
//   return result
// }

// export const all = (arr: number[], pred?: (x: number) => boolean): boolean => {
//   if (!pred) {
//     return true
//   }
//   for (const i of arr) {
//     if (!pred(i)) {
//       return false
//     }
//   }
//   return true
// }

// export const exists = (
//   arr: number[],
//   pred?: (x: number) => boolean,
// ): boolean => {
//   if (!pred) {
//     return true
//   }
//   for (const i of arr) {
//     if (pred(i)) {
//       return true
//     }
//   }
//   return false
// }

// export const compact = <T>(arr: T[]): T[] => {
//   const result: T[] = []
//   for (const i of arr) {
//     if (i) {
//       result.push(i)
//     }
//   }
//   return result
// }

// export const max2 = (x: number, y: number): number => (x > y ? x : y)

// export function maxBy<T>(arr: T[], pred: (x: T) => number): number {
//   const numbers = arr.map(e => pred(e))
//   return numbers.reduce((acc, v) => max2(acc, v), 0)
// }

// export function objectFromPairs(arr: [any, any][]): any {
//   return arr.reduce((acc, v) => {
//     // eslint-disable-next-line
//     acc[v[0]] = v[1]
//     return acc
//   }, {})
// }

// export function objectToPairs<T>(obj: T): any[][] {
//   const result = []
//   for (const k of Object.keys(obj)) {
//     result.push([k, obj[k]])
//   }
//   return result
// }

// export function pluck<T, K extends keyof T>(arr: T[], key: K): T[K][] {
//   return arr.map(it => it[key])
// }

// export const chunk = (arr: number[], size: number): number[][] => {
//   const result = []
//   for (let i = 0; i < arr.length; i += size) {
//     result.push(arr.slice(i, i + size))
//   }
//   return result
// }

// export const deepFlatten = (arr: any[]): any[] => {
//   const result = []
//   for (const e of arr) {
//     if (Array.isArray(e)) {
//       result.push(...deepFlatten(e))
//     } else {
//       result.push(e)
//     }
//   }
//   // eslint-disable-next-line @typescript-eslint/no-unsafe-return
//   return result
// }

// export const dropWhile = (
//   arr: number[],
//   pred: (n: number) => boolean,
// ): number[] => arr.filter(it => pred(it))

// export const groupBy = <T>(arr: T[], fn: (x: T) => any): any => {
//   const result = {}
//   for (const e of arr) {
//     const key = fn(e)
//     if (key in result) {
//       // eslint-disable-next-line
//       result[key].push(e)
//     } else {
//       result[key] = [e]
//     }
//   }
//   return result
// }

// export const zip2 = <T1, T2>(arr1: T1[], arr2: T2[]): any => {
//   const result = []
//   for (let i = 0; i < arr1.length; i += 1) {
//     result.push([arr1[i], arr2[i]])
//   }
//   return result
// }

// export const zipObject = <T>(arr1: (string | number)[], arr2: T[]): any => {
//   const result = {}
//   for (let i = 0; i < arr1.length; i += 1) {
//     result[arr1[i]] = arr2[i]
//   }
//   return result
// }

// // shuffle
// // deep get
// // merge
// // once
// // compose

// export const pick = <T>(obj: T, arr: string[]): any => {
//   const result = {}
//   for (const key of Object.keys(obj)) {
//     if (arr.includes(key)) {
//       result[key] = obj[key]
//     }
//   }
//   return result
// }

// export const omit = <T>(obj: T, arr: string[]): any => {
//   const result = {}
//   for (const key of Object.keys(obj)) {
//     if (!arr.includes(key)) {
//       result[key] = obj[key]
//     }
//   }
//   return result
// }

// const subtract = (x: number, y: number): number => x - y

// // eslint-disable-next-line
// export const flip = (fn: any) => (...args: any) => fn(args.reverse())

// export const deepGet = (data: any, arr: any[]): any => {
//   let val = data
//   for (const e of arr) {
//     // eslint-disable-next-line
//     val = val[e]
//   }
//   // eslint-disable-next-line @typescript-eslint/no-unsafe-return
//   return val
// }

// export const largestOfThree = (x: number, y: number, z: number): number =>
//   max2(max2(x, y), z)

// export const partition = (arr: number[], value: number): number[][] => {
//   const res1 = []
//   const res2 = []
//   let result = []
//   for (let e of arr) {
//     if (e < value) {
//       res1.push(e)
//     }
//     if (e > value) {
//       res2.push(e)
//     }
//   }
//   result = [res1, res2]
//   return result
// }

// export const mergeSortedArrays = (ar1: number[], ar2: number[]): any => {
//   const result = []
//   let i = 0
//   let j = 0
//   while (i < ar1.length && j < ar2.length) {
//     if (ar1[i] < ar2[j]) {
//       result.push(ar1[i])
//       i += 1
//     } else {
//       result.push(ar2[j])
//       j += 1
//     }
//   }
//   return [...result, ...ar1.slice(i), ...ar2.slice(j)]
// }

// export const reverse = (arr: any[]): any[] => {
//   const result = []
//   for (let i = arr.length - 1; i >= 0; i -= 1) {
//     result.push(arr[i])
//   }
//   // eslint-disable-next-line @typescript-eslint/no-unsafe-return
//   return result
// }

// export const pairs = (arr: number[]): number[][] => {
//   const result = []
//   for (let i = 0; i < arr.length - 1; i += 1) {
//     result.push([arr[i], arr[i + 1]])
//   }
//   return result
// }

// export const isPrime = (n: number): boolean => {
//   if (n === 1) {
//     return true
//   }
//   for (let i = 2; i < n; i += 1) {
//     if (n % i === 0) {
//       return false
//     }
//   }
//   return true
// }

// export const sumOfPrimes = (n: number): number => {
//   let sum = 0
//   for (let i = 2; i < n; i += 1) {
//     if (isPrime(i)) {
//       sum = sum + i
//     }
//   }
//   return sum
// }

// export const partitionByValue = (
//   arr: number[],
//   index: number,
// ): [number[], number[]] => {
//   const res1 = arr.slice(0, index)
//   const res2 = arr.slice(index)
//   return [res1, res2]
// }

// export const mergeSort = (arr: number[]): number[] => {
//   const index = Math.floor(arr.length / 2)
//   if (arr.length <= 1) {
//     return arr
//   }
//   const [first, second] = partitionByValue(arr, index)
//   const res1 = mergeSort(first)
//   const res2 = mergeSort(second)
//   // eslint-disable-next-line @typescript-eslint/no-unsafe-return
//   return mergeSortedArrays(res1, res2)
// }

// export const candies = (n: number, m: number): number => n * Math.floor(m / n)

// export const depositProfit = (
//   p: number,
//   t: number,
//   r: number,
//   n: number,
// ): any => {
//   const result = {}
//   let total = p
//   for (let i = 1; i <= n; i += 1) {
//     const intr = Math.floor((total * t * r) / 100)
//     total += intr
//     console.log(total)
//     result[`year${i}`] = total
//   }
//   return result
// }

// export const chunkyMonkey = <T>(arr: T[], ind: number): [T[], T[]] => [
//   arr.slice(0, ind),
//   arr.slice(ind),
// ]

// export const calculateCentury = (year: number): number =>
//   year % 100 === 0 ? year / 100 : Math.floor(year / 100 + 1)

// export const reverseString = (str: string): string => {
//   let result = ''
//   for (let i = str.length - 1; i >= 0; i -= 1) {
//     result += str[i]
//   }
//   return result
// }

// export const reverseString1 = (str: string): string[] =>
//   str.split('').reverse().join('')

// export const max2Str = (str1: string, str2: string): string =>
//   str1.length < str2.length ? str1 : str2

// export const maxInArray = (str: number[]): number => {
//   let max = str[0]
//   for (let i = 0; i < str.length; i += 1) {
//     const m = max2(max, str[i])
//     max = m
//   }
//   return max
// }

// export const maxInArray2 = (arr: number[]): number =>
//   arr.reduce((acc, v) => max2(acc, v), 0)

// export const sortByLength = (str: string[]): string[] =>
//   str.sort((str1, str2) => str1.length - str2.length)

// const vowels = ['a', 'e', 'i', 'o', 'u']

// export const countVowelsConsonant = (str: string): number => {
//   let sum = 0
//   for (let i = 0; i < str.length; i += 1) {
//     if (vowels.includes(str[i])) {
//       sum = sum + 1
//     } else {
//       sum = sum + 2
//     }
//   }
//   return sum
// }

// export const countVowelsConsonant2 = (str: string): number =>
//   str.split('').reduce((acc, v) => (vowels.includes(v) ? acc + 1 : acc + 2), 0)

// export const fibonacciNumbers = (n: number): number[] => {
//   const result = []
//   let prev = 0
//   let current = 1
//   while (current <= n) {
//     const total = prev + current
//     result.push(current)
//     prev = current
//     current = total
//   }
//   return result
// }

// export const isOdd = (n: number): boolean => n % 2 === 1

// export const sumOfOddFibonacci = (n: number): number =>
//   fibonacciNumbers(n)
//     .filter(v => isOdd(v))
//     .reduce((acc, v) => acc + v)

// const product = (x: number, y: number): number => x * y

// export const adjacentElementProduct = (arr: number[]): number => {
//   const result = []
//   for (let i = 0; i < arr.length - 1; i += 1) {
//     const prod = product(arr[i], arr[i + 1])
//     result.push(prod)
//   }
//   return maxInArray(result)
// }

// export const removeKth = (arr: number[], ind: number): number[] =>
//   arr.filter((v, index) => (index + 1) % ind !== 0)

// export const diff = (a: number, b: number) => a - b

// export const maximalAdjacentDiff = (arr: number[]): number => {
//   let result = []
//   for (let i = 0; i < arr.length - 1; i += 1) {
//     const sub = diff(arr[i], arr[i + 1])
//     result.push(sub)
//   }
//   return maxInArray(result)
// }

// export const insertDashes = (str: string): string => {
//   const words = str.split(' ')
//   const dashedWords = words.map(word => {
//     console.log(word)
//     const chars = word.split('')
//     console.log(chars)

//     return chars.join('-')
//   })

//   return dashedWords.join(' ')
// }

// export const differentSymbols = (str: string): number => {
//   const result: string[] = []
//   for (const e of str) {
//     if (!result.includes(e)) {
//       result.push(e)
//     }
//   }
//   return result.length
// }

// export const previousLess = (arr: number[]): number[] => {
//   const result = []
//   for (let i = 0; i < arr.length - 1; i += 1) {
//     if (arr[i + 1] > arr[i]) {
//       result.push(arr[i])
//     } else {
//       result.push(-1)
//     }
//   }
//   return result
// }

// export const alphabetSequnece = (str: string): any => {
//   const charCodes = str.split('').map(s => s.charCodeAt(0))
//   if (new Set(charCodes).size !== charCodes.length) {
//     return false
//   } else {
//     for (let i = 0; i < charCodes.length; i += 1) {
//       if (charCodes[i] < charCodes[i + 1]) {
//         return true
//       }
//       return false
//     }
//   }
//   return false
// }

// type Domain = 'com' | 'org' | 'net' | 'info'

// export const domianType = (arr: string[]): string[] => {
//   const domains = arr.map(v => v.split('.')).map(v => v[v.length - 1])
//   const result = []
//   for (const e of domains) {
//     if (e === 'org') {
//       result.push('organization')
//     } else if (e === 'com') {
//       result.push('commercial')
//     } else if (e === 'net') {
//       result.push('network')
//     } else {
//       result.push('information')
//     }
//   }
//   return result
// }

// export const sumOf2 = (
//   arr1: number[],
//   arr2: number[],
//   value: number,
// ): boolean => {
//   for (let e of arr1) {
//     for (let v of arr2) {
//       if (e + v === value) {
//         return true
//       }
//     }
//   }
//   return false
// }

// export const extracMatrixColumn = (arr: number[][], column: number): number[] =>
//   arr.map(v => v[column])
