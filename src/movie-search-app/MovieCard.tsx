import React from 'react'

export interface Movie {
  readonly title: string
  readonly img: number
  readonly imdb: string
  readonly overView: string
}

interface MovieCardProps {
  readonly movie: Movie
}

export const MovieCard: React.FC<MovieCardProps> = ({ movie }) => {
  return (
    <div className="card">
      <h2>{movie.title}</h2>
      <div className="grouped">
        <img
          src={`https://image.tmdb.org/t/p/w185_and_h278_bestv2/${movie.img}`}
          style={{ margin: '0 auto' }}
        />

        <p>{movie.imdb}</p>

        <p>{movie.overView}</p>
      </div>
    </div>
  )
}

interface MovieCardListProps {
  readonly moviesList: readonly Movie[]
}

export const MovieCardList: React.FC<MovieCardListProps> = ({ moviesList }) => {
  return (
    <div>
      {moviesList.map((m: Movie, index) => (
        <MovieCard key={index} movie={m} />
      ))}
    </div>
  )
}
