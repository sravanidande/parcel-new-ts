import React from 'react'

interface MovieSearchFormProps {
  onMovieSearch(movie: string): void
}

export const MovieSearchForm: React.FC<MovieSearchFormProps> = ({
  onMovieSearch,
}) => {
  const [searchItem, setSearchItem] = React.useState('')
  return (
    <form
      className="form"
      onSubmit={evt => {
        evt.preventDefault()
        onMovieSearch(searchItem)
      }}
    >
      <label htmlFor="query" className="label">
        Movie Name
      </label>
      <input
        id="query"
        type="text"
        name="query"
        value={searchItem}
        onChange={evt => setSearchItem(evt.target.value)}
        className="input"
        placeholder="Search movie..."
      />
      <button className="button" type="submit">
        Search
      </button>
    </form>
  )
}
