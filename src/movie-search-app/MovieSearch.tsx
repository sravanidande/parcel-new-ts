import React from 'react'
import './App.css'
import { MovieCardList } from './MovieCard'
import { MovieSearchForm } from './MovieSearchForm'

export const MovieSearchApp: React.FC = () => {
  const [movies, setMovies] = React.useState([])
  const [query, setQuery] = React.useState('')

  React.useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/search/movie?api_key=5dcf7f28a88be0edc01bbbde06f024ab&language=en-US&query=${query}&page=1&include_adult=false`,
    )
      .then(res => res.json())
      .then(data =>
        setMovies(
          data.results.map((m: any) => ({
            title: m.title,
            img: m.poster_path,
            imdb: m.vote_average,
            overView: m.overview,
          })),
        ),
      )
      .catch(err => console.error(err))
  }, [query])

  return (
    <div className="container">
      <h1 className="title">React Movie Search</h1>
      <MovieSearchForm onMovieSearch={setQuery} />
      <MovieCardList moviesList={movies} />
    </div>
  )
}
