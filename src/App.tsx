/* eslint-disable comma-dangle */
/* eslint-disable no-console */
// import { addMonths } from 'date-fns'
import React from 'react'
// import { DatePicker } from './30-days-js/DatePicker'
// import { CountDownTimer, TimerDisplay } from './30-days-js/day-5/CountdownTimer'
// import { bands, sortWithoutArticles } from './30-days-js/day-6/sorting'
// import { Cardian } from './30-days-js/day-7/Cardian'
import { MyDiary } from './30-days-js/day-8/MyDiary'
// import { TweetNote } from './30-days-js/JsXmas'
// import { MovieSearchApp } from './movie-search-app/MovieSearch'
// import { DrumKeyView, DrumkitView } from './30-days-js/day-1/DrumKey'
// import { drumKeys } from './30-days-js/day-1/data'
// import { DatePicker, RowDisplay } from './30-days-js/CalenderPicker'
// import {
//   adjacentElementProduct,
//   all,
//   alphabetSequnece,
//   calculateCentury,
//   candies,
//   chunk,
//   chunkyMonkey,
//   compact,
//   containsName,
//   countVowelsConsonant,
//   countVowelsConsonant2,
//   deepFlatten,
//   deepGet,
//   depositProfit,
//   differentSymbols,
//   divideRowsByNumber,
//   domianType,
//   dropWhile,
//   eachKth,
//   exists,
//   extracMatrixColumn,
//   fibonacciNumbers,
//   filterInventors,
//   firstDayInMonth,
//   flip,
//   groupBy,
//   insertDashes,
//   insertEmpty,
//   isOdd,
//   isPrime,
//   largestOfThree,
//   liveAllTogether,
//   max2Str,
//   maxBy,
//   maximalAdjacentDiff,
//   maxInArray,
//   maxInArray2,
//   mergeSort,
//   mergeSortedArrays,
//   objectFromPairs,
//   objectToPairs,
//   omit,
//   pairs,
//   partition,
//   partitionByValue,
//   pick,
//   pluck,
//   previousLess,
//   removeDuplicates,
//   removeKth,
//   returnNames,
//   reverse,
//   reverseString,
//   reverseString1,
//   shiftIndex,
//   sortBirthDays,
//   sortByLastName,
//   sortByLength,
//   sortByYearsLived,
//   sumOddFibonacciNumbers,
//   sumOf2,
//   sumOfAll,
//   sumOfOddFibonacci,
//   sumOfPrimes,
//   sumUpInstances,
//   zip2,
//   zipObject,
// } from './js-fns/example'
// import { Clock } from './30-days-js/day-2/Clock'

// import {
//   DatePickerView,
//   CalenderPicker,
//   range,
//   DatePickerValue,
//   YearPicker,
// } from './30-days-js/CalenderPicker'
// import { lastDayOfMonth, startOfMonth } from 'date-fns'
// import { shuffle } from 'lodash'

// const data = [
//   'car',
//   'car',
//   'truck',
//   'truck',
//   'bike',
//   'walk',
//   'car',
//   'van',
//   'bike',
//   'walk',
//   'car',
//   'van',
//   'car',
//   'truck',
// ]

// // const drumKey = { key: 83, name: 'S', sound: 'hihat' }

// const newDate = new Date(randomDate.year, randomDate.month, randomDate.day)

// console.log(newDate, 'new date')

// export const App: React.FC = () => {
//   const [date, setDate] = React.useState({ day: 1, month: 1, year: 2020 })

//   return (
//     <DatePicker
//       date={date}
//       onDateChange={date => {
//         setDate(date)
//       }}
//     />
//   )
// }

// const seconds = Date.now()

// export const App: React.FC = () => {
//   return React.createElement('div', { id: 'header' }, 'hello')
// }

export const App: React.FC = () => {
  return <MyDiary />
}

// const arr = [1, 2, 2, 3, 4, 4, 5, 6, 7, 7]

// console.log(removeDuplicates(arr))
// console.log(all([4, 2, 3], x => x > 1))
// console.log(all([1, 2, 3], x => x > 1))
// console.log(all([1, 2, 3]), 'helo')
// console.log(exists([0, 1, 2, 0], x => x >= 2))
// console.log(exists([0, 0, 1, 0]), 'bye')
// console.log(compact([0, 1, false, 2, '', 3, 'a', 'e', 23, NaN, 's', 34]))
// console.log(maxBy([{ n: 4 }, { n: 2 }, { n: 8 }, { n: 6 }], x => x.n))
// console.log(
//   objectFromPairs([
//     ['a', 1],
//     ['b', 2],
//     ['c', 3],
//   ]),
// )

// console.log(objectToPairs({ a: 1, b: 2 }))
// const simpsons = [
//   { name: 'lisa', age: 8 },
//   { name: 'homer', age: 36 },
//   { name: 'marge', age: 34 },
//   { name: 'bart', age: 10 },
// ]
// console.log(pluck(simpsons, 'age'))
// console.log(chunk([1, 2, 3, 4, 5], 2))
// console.log(deepFlatten([1, [2], [[3], 4], 5]))
// console.log(dropWhile([1, 2, 3, 4], n => n >= 3))
// console.log(groupBy([6.1, 4.2, 6.3], Math.floor))

// const stringLength = (str: string): number => str.length
// console.log(groupBy(['one', 'two', 'three'], stringLength))
// console.log(zip2(['a', 'b'], [1, 2]))
// console.log(zip2(['a'], [1, 2]))
// console.log(zipObject(['a', 'b', 'c'], [1, 2]))
// console.log(zipObject(['a', 'b'], [1, 2, 3]))
// console.log(pick({ a: 1, b: '2', c: 3 }, ['a', 'c']))
// console.log(omit({ a: 1, b: '2', c: 3 }, ['b']))
// const subtract = (x: number, y: number): any => x - y
// console.log(flip(subtract)(100, 90))
// const object = {
//   a: [{ x: 2 }, { y: 4 }],
//   b: 1,
// }
// const other = {
//   a: { z: 3 },
//   b: [2, 3],
//   c: 'foo',
// }

// const add5 = (x: number): number => x + 5
// const multiply = (x: number, y: number): number => x * y

// const index = 2
// const data = {
//   foo: {
//     foz: [1, 2, 3],
//     bar: {
//       baz: ['a', 'b', 'c'],
//     },
//   },
// }
// console.log(deepGet(data, ['foo', 'foz', index]))

// console.log(largestOfThree(89, 62, 92))
// console.log(partition([1, 4, 2, 5, 3], 3)[0])
// console.log(partition([1, 4, 2, 5, 3], 3)[1])
// console.log(mergeSortedArrays([1, 3, 5], [2, 4]), 'helo')
// console.log(reverse([1, 2, 3, 4, 5]))
// console.log(pairs([1, 2, 3, 4, 5]))
// console.log(isPrime(9))
// console.log(sumOfPrimes(15))
// // console.log(isSorted([1, 3, 2]))
// // console.log(isSorted([1, 2, 3, 5]))
// console.log(partitionByValue([1, 3, 5, 7, 9, 2], 4))
// console.log(mergeSort([1, 3, 5, 2, 4]))
// console.log(candies(3, 10))
// console.log(depositProfit(100, 1, 20, 3))
// console.log(chunkyMonkey(['a', 'b', 'c', 'd'], 2))
// console.log(chunkyMonkey([0, 1, 2, 3, 4, 5], 4))
// console.log(calculateCentury(1995))
// console.log(calculateCentury(1700))
// console.log(calculateCentury(2020))
// console.log(reverseString('sravani'))
// console.log(reverseString1('sravani'))
// console.log(max2Str('sravani', 'dande'))
// console.log(maxInArray([1, 3, 9, 5]))
// console.log(maxInArray2([12, 89, 65, 108]))
// console.log(sortByLength(['abc', '', 'aaa', 'a', 'zz']))
// console.log(countVowelsConsonant('abcde'))
// console.log(countVowelsConsonant2('abcde'))
// console.log(fibonacciNumbers(10))
// console.log(sumOfOddFibonacci(10))
// console.log(isOdd(2))
// console.log(adjacentElementProduct([3, 6, -2, -5, 7, 3]))
// console.log(removeKth([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3))
// console.log(maximalAdjacentDiff([2, 4, 1, 0]))
// console.log(maximalAdjacentDiff([2, 9, 1, 0]))
// console.log(insertDashes('aba caba'))
// console.log(differentSymbols('cabca'))
// console.log(previousLess([3, 5, 2, 4, 5]))
// console.log(alphabetSequnece('ace'))
// console.log(
//   domianType(['en.wiki.org', 'codefights.com', 'happy.net', 'code.info']),
// )
// console.table(filterInventors(inventors))
// console.log(returnNames(inventors))
// console.log(sortBirthDays(inventors))
// // const data1 = ['car', 'car', 'bike']
// console.log(sumUpInstances(data))
// console.log(sortByLastName(people))
// const parisData = ['hsuhd', 'lehsuh', 'jsleis']
// console.log(containsName(parisData))
// console.log(sortByYearsLived(inventors))
// console.log(liveAllTogether(inventors))
// console.log(sumOf2([1, 2, 3], [10, 20, 30, 40], 66))
// console.log(
//   extracMatrixColumn(
//     [
//       [1, 1, 1, 2],
//       [0, 5, 0, 4],
//       [2, 1, 3, 6],
//     ],
//     2,
//   ),
// )
// console.log(divideRowsByNumber([1, 2, 3, 4, 5, 6, 7, 8, 9], 3))

// const date = lastDayOfMonth(new Date(2020, 11))
// console.log(startOfMonth(new Date()).getDay())
// console.log(startOfMonth(randomDate).getDay())
// console.log(insertEmpty([1, 2, 3, 4, 5], 2))
